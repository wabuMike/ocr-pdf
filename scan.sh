#!/bin/bash
cd /ocrpdf
while true
do
    if [ "$(ls -A input)" ]; then
        for pdfFile in input/*.pdf
        do
            if mkdir "$pdfFile.lock.d"; then
                echo "Reading pdf $pdfFile"
            
                pdfbase=$(basename "$pdfFile" .pdf)
                
                # extract images
                pdfimages "$pdfFile" tmpImages
                
                # ocr on images
                for image in tmpImages-*.ppm
                do
                    base=$(basename "$image" .ppm)
                    echo "OCR on $base"
                    tesseract -l eng+deu "$image" "${base}" pdf
                done
                
                # merge images
                echo "Merge all pages into one pdf"
                pdftk tmp*.pdf output "output/${pdfbase}.pdf"
                
                # delete temporary files
                echo "Clean up temporary files"
                rm tmp*.pdf tmp*.ppm
                rm $pdfFile
                rmdir "$pdfFile.lock.d"
            else
                echo "File $pdfFile locked by other scan process"
            fi
        done
    fi
    sleep 1m
done