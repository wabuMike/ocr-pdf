[![pipeline status](https://gitlab.com/wabuMike/ocr-pdf/badges/master/pipeline.svg)](https://gitlab.com/wabuMike/ocr-pdf/-/commits/master)

# OCR-PDF

## Makes pdf files searchable

### Why?

When you scan or photograph documents, you likely save them as [pdf](https://en.wikipedia.org/wiki/PDF)-files. To allow for text search in your favorite pdf-viewer, the text of your scanned document needs to be extracted.

### What is it doing?

A bash script extracts images from the pdf file. [tesseract](https://github.com/tesseract-ocr/tesseract) is used to extract text from those images and [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) merges those files back together.

### How?

A Docker image is provided in the GitLab Container Registry of this project. It is built for amd64, armv7l and arm64.

1. [Install Docker](https://docs.docker.com/install/)
2. Create one directory for pdf input and one directory for output (i.e. `mkdir ~/input`, `mkdir ~/output`). The input directory will serve as a hotfolder. Once you paste pdf files into your input directory, they will be processed. When all ocr processing is done, they will be copied to the output directory.
3. Run the docker container `sudo docker run --mount type=bind,source=<your input directory>,target=/ocrpdf/input --mount type=bind,source=<your output directory>,target=/ocrpdf/output -d registry.gitlab.com/wabumike/ocr-pdf:master`

### Credits

The blog post on [auxnet](https://www.auxnet.de/sandy-skript-zum-erstellen-von-sandwich-pdfs-unter-ubuntu-14-04/) gave lots of inspiration.

### Alternatives

* [https://github.com/jbarlow83/OCRmyPDF](https://github.com/jbarlow83/OCRmyPDF)

### Restrictions

* Currently works with English and German only. Feel free to create merge requests with other languages!

### License

[AGPL v3](https://www.gnu.org/licenses/agpl-3.0.en.html)