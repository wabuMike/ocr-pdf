FROM ubuntu:20.04

RUN apt-get update \
    && apt-get --reinstall install libc-bin \
    && apt-get install -y \
        pdftk \
        poppler-utils \
        tesseract-ocr \
        tesseract-ocr-deu \
        tesseract-ocr-eng \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir ocr-pdf

COPY . ocr-pdf

ENTRYPOINT ["ocr-pdf/scan.sh"]